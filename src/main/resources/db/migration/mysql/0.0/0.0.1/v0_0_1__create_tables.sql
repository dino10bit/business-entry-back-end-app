use businessDB;

create table if not exists business
(
    id bigint auto_increment primary key,
    address varchar(255) null,
    name    varchar(255) null
);

create table if not exists working_days
(
    id bigint auto_increment primary key,
    day_of_week int null
);

create table if not exists business_opening_days
(
    business_id     bigint not null,
    opening_days_id bigint not null,
    constraint UK_cn38djlsugw0395hmo2c3wcan
        unique (opening_days_id),
    constraint FKkvhqnj6pwudfsftecs8ai6ag0
        foreign key (opening_days_id) references working_days (id),
    constraint FKmlfr8ljnml7f99b1o6a2iwnqs
        foreign key (business_id) references business (id)
);

create table if not exists opening_times
(
    id bigint auto_increment primary key,
    closing_hour varchar(255) null,
    opening_hour varchar(255) null,
    type         varchar(255) null
);



create table if not exists working_days_opening_times
(
    working_day_id   bigint not null,
    opening_times_id bigint not null,
    constraint UK_8m9ck3saltns80ei95inr7acs
        unique (opening_times_id),
    constraint FK3hf2g3e599752osokgmx381dg
        foreign key (working_day_id) references working_days (id),
    constraint FKr62afxl3nbyuik7eosio18lck
        foreign key (opening_times_id) references opening_times (id)
);
