use businessDB;

insert into business (id, name, address)
VALUES (1, 'Cafe Marcella', 'Amstelveld 21, 1017 JD Amsterdam, Netherlands'),
       (2, 'Café de Sluyswacht', 'Jodenbreestraat 1, 1011 NG Amsterdam, Netherlands'),
       (3, 'Bar Bouche', 'Wibautstraat 107H, 1091 GL Amsterdam, Netherlands');

insert into working_days (id, day_of_week)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 5),
       (6, 6),
       (7, 0),
       (8, 1),
       (9, 2),
       (10, 3),
       (11, 4),
       (12, 0),
       (13, 1),
       (14, 2),
       (15, 3),
       (16, 4),
       (17, 5),
       (18, 6);


insert into business_opening_days (business_id, opening_days_id)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (1, 5),
       (1, 6),
       (2, 7),
       (2, 8),
       (2, 9),
       (2, 10),
       (2, 11),
       (3, 12),
       (3, 13),
       (3, 14),
       (3, 15),
       (3, 16),
       (3, 17),
       (3, 18);

insert into opening_times(id, closing_hour, opening_hour, type)
VALUES (1, '15:00', '11:30', 'OPEN'),
       (2, '00:00', '18:30', 'OPEN'),
       (3, '15:00', '11:30', 'OPEN'),
       (4, '00:00', '18:30', 'OPEN'),
       (5, '15:00', '11:30', 'OPEN'),
       (6, '00:00', '18:30', 'OPEN'),
       (7, '15:00', '11:30', 'OPEN'),
       (8, '00:00', '18:30', 'OPEN'),
       (9, '00:00', '18:00', 'OPEN'),
       (10, '15:00', '11:30', 'OPEN'),
       (11, '14:00', '11:30', 'OPEN'),
       (12, '22:00', '18:30', 'OPEN'),
       (13, '14:00', '11:30', 'OPEN'),
       (14, '22:00', '18:30', 'OPEN'),
       (15, '14:00', '11:30', 'OPEN'),
       (16, '22:00', '18:30', 'OPEN'),
       (17, '14:00', '12:30', 'OPEN'),
       (18, '22:00', '18:30', 'OPEN'),
       (19, '14:00', '12:30', 'OPEN'),
       (20, '22:00', '18:30', 'OPEN'),
       (21, '16:30', '8:00', 'OPEN'),
       (22, '16:30', '8:00', 'OPEN'),
       (23, '14:00', '11:30', 'OPEN'),
       (24, '22:00', '18:30', 'OPEN'),
       (25, '15:00', '11:30', 'OPEN'),
       (26, '22:00', '18:30', 'OPEN'),
       (27, '16:30', '8:00', 'CLOSED'),
       (28, '13:30', '8:00', 'OPEN'),
       (29, '13:30', '8:00', 'CLOSED');


insert into working_days_opening_times(working_day_id, opening_times_id)
VALUES (1, 1),
       (1, 2),
       (2, 3),
       (2, 4),
       (3, 5),
       (3, 6),
       (4, 7),
       (4, 8),
       (5, 9),
       (6, 10),
       (7, 11),
       (7, 12),
       (8, 13),
       (8, 14),
       (9, 15),
       (9, 16),
       (10, 17),
       (10, 18),
       (11, 19),
       (11, 20),
       (12, 21),
       (12, 22),
       (13, 23),
       (13, 24),
       (14, 25),
       (15, 26),
       (16, 27),
       (17, 28),
       (18, 29);