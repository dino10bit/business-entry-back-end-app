package app.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class BusinessDto {
    private Long Id;
    private String name;
    private String address;
    private List<WorkingDayDto> openingDays = new ArrayList<>();
}
