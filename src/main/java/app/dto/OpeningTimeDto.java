package app.dto;

import app.enums.OpeningHoursType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpeningTimeDto {
    private String openingHour;
    private String closingHour;
    private OpeningHoursType type;
}
