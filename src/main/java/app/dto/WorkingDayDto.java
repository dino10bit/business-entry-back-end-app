package app.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class WorkingDayDto {
    private DayOfWeek dayOfWeek;
    private List<OpeningTimeDto> openingTimes = new ArrayList<>();
}
