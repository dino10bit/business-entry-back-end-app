package app.mapper;

import app.dto.BusinessDto;
import app.model.Business;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface BusinessMapper {
    BusinessMapper INSTANCE = Mappers.getMapper(BusinessMapper.class);

    BusinessDto mapToDto(Business business);
    Business mapToModel(BusinessDto businessDto);
}
