//package app.service.impl;
//
//import app.dto.PlaceDto;
//import app.exception.ResourceNotFoundException;
//import app.model.Place;
//import app.repository.PlacesRepository;
//import app.search.ProductSearchCriteria;
//import app.service.PlacesService;
//import app.validation.PlacesDtoValidator;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Pageable;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Service
//public class PlacesServiceImpl implements PlacesService {
//
//    private final PlacesRepository placesRepository;
//    private final PlacesDtoValidator placesDtoValidator;
//
//    @Autowired
//    public PlacesServiceImpl(
//            PlacesRepository placesRepository,
//            PlacesDtoValidator placesDtoValidator) {
//        this.placesRepository = placesRepository;
//        this.placesDtoValidator = placesDtoValidator;
//    }
//
//    @Override
//    public PlaceDto findById(Long id) {
//        Place place = this.placesRepository
//                .findById(id)
//                .orElseThrow(() -> new ResourceNotFoundException(id));
//
//        return new PlaceDto(place);
//    }
//
//    @Override
//    public List<PlaceDto> findAll(Pageable pageable) {
//        return this.placesRepository
//                .findAll()
//                .stream()
//                .map(places -> new PlaceDto(places))
//                .collect(Collectors.toList());
//    }
//
//    @Override
//    public PlaceDto save(PlaceDto placeDto) {
//        placesDtoValidator.validate(placeDto);
//        Place place = this.dtoToEntity(placeDto);
//        Place savedPlace = this.placesRepository.save(place);
//        return new PlaceDto(savedPlace);
//    }
//
//    @Override
//    public void deleteById(Long id) {
//        this.placesRepository.deleteById(id);
//    }
//
//    @Override
//    public void search(ProductSearchCriteria criteria) {
////        PageSearchResult<Product> page = this.productRepository.search(criteria);
////        List<ProductDto> dtos = page
////                .getPageData()
////                .stream()
////                .map(ProductDto::new)
////                .collect(Collectors.toList());
////
////        return new PageSearchResult<>(page.getTotalRows(), dtos);
//    }
//
//    private Place dtoToEntity(PlaceDto placeDto) {
//        Place place = new Place();
//        BeanUtils.copyProperties(placeDto, place);
//        return place;
//    }
//}
