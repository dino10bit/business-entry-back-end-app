package app.service;

import app.exception.BusinessNotFoundException;
import app.dto.BusinessDto;
import app.mapper.BusinessMapper;
import app.model.Business;
import app.repository.BusinessRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class BusinessService {
    private final BusinessRepository businessRepository;
    private final BusinessMapper businessMapper;

    @Transactional
    public List<BusinessDto> showAll() {
        var allBusiness = businessRepository.findAll();
        return allBusiness.stream().map(businessMapper::mapToDto).collect(Collectors.toList());
    }

    @Transactional
    public BusinessDto findById(Long id) {
        Business business = businessRepository.findById(id).orElseThrow(() -> new BusinessNotFoundException("For id " + id));
        return businessMapper.mapToDto(business);
    }

    @Transactional
    public void createBusiness(BusinessDto businessDto) {
        Business business = businessMapper.mapToModel(businessDto);
        businessRepository.save(business);
    }

    @Transactional
    public Business updateBusiness(Business business) {
        var foundBusiness = businessRepository.findById(business.getId()).orElseThrow(() -> new BusinessNotFoundException("For id " + business.getId()));
        foundBusiness.setName(business.getName());
        foundBusiness.setAddress(business.getAddress());
        businessRepository.save(foundBusiness);

        return foundBusiness;
    }

    @Transactional
    public void deleteAll() {
        businessRepository.deleteAll();
    }

    @Transactional
    public void deleteById(Long id) {
        businessRepository.deleteById(id);
    }

}
