package app.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
@Table(name = "working_days")
public class WorkingDay {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private DayOfWeek dayOfWeek;

    @OneToMany(cascade = CascadeType.ALL)
    private List<OpeningTime> openingTimes = new ArrayList<>();

    public WorkingDay(DayOfWeek dayOfWeek, List<OpeningTime> openingTimes) {
        this.dayOfWeek = dayOfWeek;
        this.openingTimes = openingTimes;
    }
}




