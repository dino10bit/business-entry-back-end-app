package app.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "business")
@Data
public class Business {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    private String address;

    @OneToMany(cascade = CascadeType.ALL)
    private List<WorkingDay> openingDays = new ArrayList<>();

    public void addOpeningHour(WorkingDay workingDay) {
        openingDays.add(workingDay);
    }
}
