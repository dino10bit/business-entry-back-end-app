package app.model;

import app.enums.OpeningHoursType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Table(name = "opening_times")
public class OpeningTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String openingHour;
    private String closingHour;

    @Enumerated(EnumType.STRING)
    private OpeningHoursType type;

    public OpeningTime(String openingHour, String closingHour,  OpeningHoursType type) {
        this.openingHour = openingHour;
        this.closingHour = closingHour;
        this.type = type;
    }
}
