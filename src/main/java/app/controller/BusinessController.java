package app.controller;

import app.dto.BusinessDto;
import app.mapper.BusinessMapper;
import app.model.Business;
import app.repository.BusinessRepository;
import app.service.BusinessService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/business")
public class BusinessController {

    private final BusinessService businessService;
    private final BusinessRepository businessRepository;
    private final BusinessMapper businessMapper;

    @GetMapping(value = "/all", produces = "application/json")
    public ResponseEntity<List<BusinessDto>> getAllBusinesses(@RequestParam(required = false) String name) {
        try {
            List<BusinessDto> businesses = new ArrayList<>();

            if (name == null) {
                businesses.addAll(businessService.showAll());
            } else {
                var businessList =  businessRepository.findByNameContains(name);
                businessList.stream().map(businessMapper::mapToDto).forEach(businesses::add);
            }

            if (businesses.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(businesses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<BusinessDto> getBusiness(@PathVariable @RequestBody Long id) {
        return new ResponseEntity<>(businessService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<HttpStatus> createBusiness(@RequestBody BusinessDto businessDto) {
        businessService.createBusiness(businessDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Business> updateBusiness(@RequestBody Business business) {
        return new ResponseEntity<>(businessService.updateBusiness(business), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<HttpStatus> deleteBusiness(@PathVariable("id") long id) {
        try {
            businessService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping
    public ResponseEntity<HttpStatus> deleteAllBusinesses() {
        try {
            businessService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
