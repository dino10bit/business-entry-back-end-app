package app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {
    @GetMapping("/")
    public String index(){
        return "Welcome to our business API </br></br>\n" +
                "<a href=\"http://localhost:8080/api/swagger-ui.html\">See Swagger</a>";
    }
}
