package app.controller;

import app.enums.OpeningHoursType;
import app.model.Business;
import app.model.OpeningTime;
import app.model.WorkingDay;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.Nullable;
import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BusinessControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void whenPostRequest_thenCreateBusinessEntity() throws Exception {
        var expectedBusiness = new Business();
        expectedBusiness.setName("Test name");
        expectedBusiness.setAddress("Test address");

        setOpeningTimes(expectedBusiness, DayOfWeek.MONDAY,
                "8:00", "16:30", OpeningHoursType.OPEN, null, null, null);
        setOpeningTimes(expectedBusiness, DayOfWeek.TUESDAY,
                "11:30", "14:00", OpeningHoursType.OPEN, "18:30", "22:00", OpeningHoursType.OPEN);
        setOpeningTimes(expectedBusiness, DayOfWeek.WEDNESDAY,
                "11:30", "14:00", OpeningHoursType.CLOSED, "18:30", "22:00", OpeningHoursType.OPEN);
        setOpeningTimes(expectedBusiness, DayOfWeek.THURSDAY,
                "11:30", "14:00", OpeningHoursType.OPEN, "18:30", "22:00", OpeningHoursType.OPEN);
        setOpeningTimes(expectedBusiness, DayOfWeek.FRIDAY,
                "11:30", "14:00", OpeningHoursType.OPEN, "18:30", "22:00", OpeningHoursType.CLOSED);

        this.mockMvc.perform(post("/business")
                .content(asJsonString(expectedBusiness))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        var getResponse = this.mockMvc.perform(get("/business/all")
                .param("name", "Test"))
                .andExpect(status().is(200))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        var getResponseAsObject = new JSONArray(getResponse).getJSONObject(0);

        ObjectMapper objectMapper = new ObjectMapper();
        var actualBusiness = objectMapper.readValue(getResponseAsObject.toString(), Business.class);

        assertEquals(actualBusiness.getName(), expectedBusiness.getName());
        assertEquals(actualBusiness.getAddress(), expectedBusiness.getAddress());

        this.mockMvc.perform(delete("/business/" + actualBusiness.getId()))
                .andExpect(status().is(204));
    }

    private void setOpeningTimes(Business business, DayOfWeek dayOfWeek,
                                 String openingTime1, String closingTime1, OpeningHoursType type1,
                                 @Nullable String openingTime2, @Nullable String closingTime2, @Nullable OpeningHoursType type2) {
        List<OpeningTime> openingTimesList = new ArrayList<>();

        var firstOpeningTime = new OpeningTime(openingTime1, closingTime1, type1);
        openingTimesList.add(firstOpeningTime);

        if (openingTime2 != null && closingTime2 != null && type2 != null) {
            var secondOpeningTime = new OpeningTime(openingTime2, closingTime2, type2);
            openingTimesList.add(secondOpeningTime);
        }

        business.addOpeningHour(new WorkingDay(dayOfWeek, openingTimesList));
    }
}
